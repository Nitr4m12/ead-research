#!/usr/bin/env python
from struct import Struct
from pathlib import Path
from sys import argv
from typing import Tuple, List

from binary_reader import Reader
import oead

class Dummy():
    ...

class PTCL:
    def __init__(self, data: bytes, endian: str):
        self.stream = Reader(data, endian)
        self.header: self.Header = self.Header(self.stream)
        self.sections = []

        # Move 0x24 bytes to the first section
        
        
        self.esta: SubSection = SubSection(self.stream)
        self.eset: ESET = ESET(self.stream)

    class Header():
        def __init__(self, stream: Reader):
            self.magic = stream.read_string(4)
            assert self.magic in [b"EFTB", b"VFXB"]

            if self.magic == b"EFTB":
                self.vfx_api_version = stream.read_uint32()
                self.game = stream.read_string(4)
                stream.skip(0x24)

            elif self.magic == b"VFXB":
                self.signature = stream.read_uint32()
                assert self.signature == 0x20202020

                self.graphics_api_version = stream.read_uint16()
                self.vfx_api_version = stream.read_uint16()
                self.bom = stream.read_uint16()
                self.alignment = stream.read_uint8()
                self.target_offset = stream.read_uint8()
                self.header_size = stream.read_uint32()
                self.flag = stream.read_uint16()
                self.block_offset = stream.read_uint16()
                stream.skip(4)
                self.file_size = stream.read_uint32()
                stream.skip(self.block_offset - self.header_size)

            else:
                raise NotImplementedError(f'io handling not implemented for {self.magic.decode("ascii")}')

class SubSection():
    def __init__(self, stream: Reader, get_data: bool = False):
        start = stream.tell()
        self.magic: bytes = stream.read_string(4)
        assert self.magic in [b"ESTA", b"ESET", b"EMTR"]

        self.section_size: int = stream.read_uint32()
        self.subsection_offset: int = start + stream.read_uint32()
        self.next_section_offset: int = start + stream.read_uint32()
        self.unknown: int = stream.read_uint32()
        self.data_offset: int = start + stream.read_uint32()
        stream.skip(4)
        self.subsection_count: int = stream.read_uint16()
        self.unknown2: int = stream.read_uint16()

        if get_data:
            self.data: SubSectionData = SubSectionData(self, stream)

class SubSectionData():
    def __init__(self, section: SubSection, stream: Reader):     

        self.switch = {
            "ESTA": load_esta,
            "ESET": load_eset,
        }

        self.section_data = self.switch[section.magic.decode("ascii")](section, buffer, offset)

class ESET(SubSection):
    def __init__(self, stream: Reader):
        super().__init__(stream)

        stream.skip(16)
        self.emitter_name = stream.read_string(64)
        stream.skip(16)
        self.emitters = []
        self.emtr = self.EMTR(stream)

    class EMTR(SubSection):
        def __init__(self, stream: Reader):
            super().__init__(stream)

            unk_data: List[int] = []
            
            stream.seek(0x110) # Align to 0x20 bytes
            self.attrib_name: bytes = stream.read_string(64)
            stream.skip(0x10) # padding
            self.color0_key_count: int = stream.read_uint32()
            self.alpha0_key_count: int = stream.read_uint32()
            self.color1_key_count: int = stream.read_uint32()
            self.alpha1_key_count: int = stream.read_uint32()
            self.scale_key_count: int = stream.read_uint32()
            self.unknown_count: int = stream.read_uint32()
            stream.skip(0x38) # padding

            for i in range(12):
                # Read 12 32-bit ints
                self.unk_data.append(stream.read_uint32())

            self.blink_intensity0: float = stream.read_float()
            self.blink_intensity1: float = stream.read_float()
            self.blink_duration0: float = stream.read_float()
            self.blink_duration1: float = stream.read_float()
            

            for i in range(176):
                # Read 176 32-bit ints
                unk_data.append(stream.read_uint32())
            
            self.radius: float = stream.read_float()
            self.radius_copy: float = stream.read_float()
            stream.skip(0x08)

            self.color0_array: List[Dummy] = []
            self.color0_alpha_array: List[Dummy] = []
            self.color1_array: List[Dummy] = []
            self.color1_alpha_array: List[Dummy] = []
            self.scale_array: List[Dummy] = []
            
            for i in range(8):
                color0 = Dummy()
                color0.r: float = stream.read_float()
                color0.g: float = stream.read_float()
                color0.b: float = stream.read_float()
                color0.time: float = stream.read_float()
                self.color0_array.append(color0)

            for i in range(8):
                color0_alpha = Dummy()
                color0_alpha.r: float = stream.read_float()
                color0_alpha.g: float = stream.read_float()
                color0_alpha.b: float = stream.read_float()
                color0_alpha.time: float = stream.read_float()
                self.color0_alpha_array.append(color0_alpha)

            for i in range(8):
                color1 = Dummy()
                color1.r: float = stream.read_float()
                color1.g: float = stream.read_float()
                color1.b: float = stream.read_float()
                color1.time: float = stream.read_float()
                self.color1_array.append(color1)

            for i in range(8):
                color1_alpha = Dummy()
                color1_alpha.r: float = stream.read_float()
                color1_alpha.g: float = stream.read_float()
                color1_alpha.b: float = stream.read_float()
                color1_alpha.time: float = stream.read_float()
                self.color0_alpha_array.append(color1_alpha)

            for i in range(16):
                self.unk_data.append(stream.read_uint32())

            for i in range(8):
                scale = Dummy()
                scale.r: float = stream.read_float()
                scale.g: float = stream.read_float()
                scale.b: float = stream.read_float()
                scale.time: float = stream.read_float()
                self.scale_array.append(scale)
            print(stream.tell())
            for i in range(52):
                unk_data.append(stream.read_uint32())

            for i in range(16):
                unk_data.append(stream.read_uint8())

            for i in range(36):
                unk_data.append(stream.read_uint32())

            for i in range(8):
                unk_data.append(stream.read_uint8())

            for i in range(16):
                unk_data.append(stream.read_uint32())

            for i in range(8):
                unk_data.append(stream.read_uint8())

            for i in range(22):
                unk_data.append(stream.read_uint32())

            for i in range(8):
                unk_data.append(stream.read_uint8())

            unk_data.append(stream.read_uint32())
            unk_data.append(stream.read_uint32())

            for i in range(0x10):
                unk_data.append(stream.read_uint8())

            for i in range(6):
                unk_data.append(stream.read_uint32())

            for i in range(0x10):
                unk_data.append(stream.read_uint8())

            for i in range(6):
                unk_data.append(stream.read_uint32())

            for i in range(0x18):
                unk_data.append(stream.read_uint8())

            for i in range(2):
                unk_data.append(stream.read_uint32())

            stream.skip(0x4) # Unknown
            unk_ptr1 = stream.read_uint64()
            unk_data.append(unk_ptr1)
            stream.skip(0x4) # Unknown

            unk_data.append(stream.read_uint32())
            unk_data.append(stream.read_uint32())

            unk_ptr2 = stream.read_uint64()
            unk_data.append(unk_ptr2)

            unk_data.append(stream.read_uint32())
            unk_data.append(stream.read_uint32())
            unk_data.append(stream.read_uint32())
            unk_data.append(stream.read_uint32())

            self.atest_only = stream.read_string(10)
            # print(self.atest_only)
            
            stream.skip(2) # Align

            for i in range(18):
                unk_data.append(stream.read_uint32())

            for i in range(12):
                unk_data.append(stream.read_uint8())

            # Read the constant colors
            self.constant_color0 = Dummy()
            self.constant_color0.r: float = stream.read_float()
            self.constant_color0.g: float = stream.read_float()
            self.constant_color0.b: float = stream.read_float()
            self.constant_color0.a: float = stream.read_float()

            self.constant_color1 = Dummy()
            self.constant_color1.r: float = stream.read_float()
            self.constant_color1.g: float = stream.read_float()
            self.constant_color1.b: float = stream.read_float()
            self.constant_color1.a: float = stream.read_float()
            
            for i in range(12):
                unk_data.append(stream.read_uint32())

            self.samplers = []
           
            for i in range(3):
                sampler = self.SamplerInfo(stream)
                self.samplers.append(sampler)
            
            for i in range(0x30):
                unk_data.append(stream.read_uint8())
                
            # print(stream.tell())
            print(stream.read_string(4))

        class SamplerInfo:
            def __init__(self, stream: Reader):
                self.flags = []

                self.enabled: bool = stream.read_uint32() == 0
                self.texture_id: int = stream.read_uint32()
                self.flags.append(stream.read_uint8())
                assert self.flags[0] in [0, 1, 2]

                self.flags.append(stream.read_uint8())
                assert self.flags[1] in [0, 1, 2]

                stream.skip(0x01) # padding

                self.flags.append(stream.read_uint8())
                assert self.flags[2] in [0, 1]

                self.unk_float: float = stream.read_float()
                assert round(self.unk_float, 2) in [2.0, 6.0, 15.99]

                stream.skip(0x04) # padding

                self.flags.append(stream.read_uint8())
                assert self.flags[3] in [0, 1]

                self.flags.append(stream.read_uint8())
                assert self.flags[4] in [0, 1]

                self.flags.append(stream.read_uint8())
                assert self.flags[5] in [0, 1]

                self.flags.append(stream.read_uint8())
                assert self.flags[6] in [0, 1]

                self.flags.append(stream.read_uint8())
                assert self.flags[7] in [0, 1]

                stream.skip(0x07)

class TEXA(SubSection):
    def __init__(self, stream: Reader):
        super().__init__(stream)

        self.textures = []
        self.texr = self.TEXR(stream)

    class TEXR(SubSection):
        def __init__(self, stream: Reader):
            super().__init__(stream)

            self.width = stream.read_uint16()
            self.height = stream.read_uint16()
            unknown1 = stream.read_uint32()
            self.comp_sel = stream.read_uint32()
            self.mip_count = stream.read_uint32()
            unknown2 = stream.read_uint32()
            self.tile_mode = stream.read_uint32()
            unknown3 = stream.read_uint32()
            self.image_size = stream.read_uint32()
            unknown4 = stream.read_uint32()
            self.texture_id = stream.read_uint32()
            self.surf_format = stream.read_uint8()
            unknown5 = stream.read_uint8()
            unknown6 = stream.read_uint16()
            unknown7 = stream.read_uint32()

all_files = Path(argv[1])
# esetlist = Path(argv[1])
# file = PTCL(esetlist.read_bytes(), "little")
            
for file in all_files.rglob("*.sesetlist"):
    esetlist = oead.yaz0.decompress(file.read_bytes())
    PTCL(esetlist, "big")