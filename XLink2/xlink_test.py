#!/usr/bin/env python
from binary_reader import Reader

class XLink:
    def __init__(self, data: bytes, endian: str = "little"):
        self.stream: Reader = Reader(data, endian)

    class Header:
        def __init__(self, stream: Reader):
            magic = stream.read_string(4)
            assert magic == b"XLNK"

            file_size = stream.read_uint32()
            version = stream.read_uint32()
            res_param_count = stream.read_uint32()